﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(OverlapDetection))]
public class AutoDamper : MonoBehaviour
{
    public OverlapDetection ovDetect;
    public float retainRate = 0.65f;
    public bool shouldDamp = true;
    public bool shouldDampRotation = false;
    private void Start()
    {
        if (!ovDetect) ovDetect = this.GetComponent<OverlapDetection>();
    }
    private void FixedUpdate()
    {
        if (!shouldDamp) return;
        foreach (var item in ovDetect.items)
        {
            EnforcableMachine em = item.GetComponent<EnforcableMachine>();
            if (em)
            {
                Vector3 v = em.rgbd.velocity.normalized * em.rgbd.velocity.magnitude * retainRate;
                //Debug.LogFormat("{0} to {1}",em.rgbd.velocity,v);
                em.rgbd.velocity = v;

                em.rgbd.AddForce(v-em.rgbd.velocity,ForceMode.VelocityChange);

                if (shouldDampRotation)
                {
                    em.rgbd.angularVelocity = em.rgbd.angularVelocity.normalized * em.rgbd.angularVelocity.magnitude * retainRate;
                }
            }
        }
    }
}
