﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationManager : MonoBehaviour
{
    public Transform target;

    public float lerpRate=15;

    Quaternion currentRotation;
    Quaternion targetRotation;

    // Start is called before the first frame update
    void Start()
    {
        currentRotation = target.rotation;
        targetRotation = target.rotation;
        InputManager.instance.onSwipe += OnSwipe;
    }
    private void OnDestroy()
    {
        InputManager.instance.onSwipe -= OnSwipe;
    }
    void OnSwipe(Dir dir)
    {
        if (MachineDropManager.instance.isDroppingShit) return;

        switch (dir)
        {
            case Dir.LEFT:
                targetRotation = targetRotation * Quaternion.Euler(0,-90,0);
                break;
            case Dir.RIGHT:

                targetRotation = targetRotation * Quaternion.Euler(0, 90, 0);
                break;
            default:
                break;
        }
    }
    void Update()
    {
        currentRotation = Quaternion.Lerp(currentRotation,targetRotation,lerpRate*Time.deltaTime);

        target.localRotation = currentRotation;
    }
}
