﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineDropManager : MonoBehaviour
{

    public event System.Action<MechType> onNewMachineBeingPicked;
    public event System.Action<BaseMachineBehaviour> onNewMachineChosen;

    public float sphereDiameter = 0.5f;
    public Camera cam;

    public LayerMask mask;

    public Transform machineRoot;

    public RectTransform machineDropPoint;
    public UnityEngine.UI.Button stopButton;
    public GameObject uiButtonPrefab;

    public List<Dictionary<MechType, MechDropData>> dropDataDictionaries = new  List<Dictionary<MechType, MechDropData>>();

    public static MachineDropManager instance;

    public MachineResetManager resetMan;
    private void Awake()
    {
        instance = this;
        resetMan = GetComponent<MachineResetManager>();

        dropDataDictionaries.Add(new Dictionary<MechType, MechDropData>());
        dropDataDictionaries.Add(new Dictionary<MechType, MechDropData>());
        dropDataDictionaries.Add(new Dictionary<MechType, MechDropData>());

        foreach (Transform tr in machineRoot)
        {
            BaseMachineBehaviour bmb = tr.GetComponent<BaseMachineBehaviour>();
            if (bmb && bmb.isPlacable)
            {
                int g = TypeToGroup(bmb.type);

                if (dropDataDictionaries[g].ContainsKey(bmb.type))
                {
                    dropDataDictionaries[g][bmb.type].count++;
                }
                else
                {
                    dropDataDictionaries[g].Add(bmb.type, new MechDropData(1, bmb));
                }
                //Debug.LogFormat("added type: {0}",bmb.type);
            }
        }

        foreach (var dic in dropDataDictionaries)
        {
            foreach (var item in dic)
            {
                GameObject uiButtonObj = Instantiate(uiButtonPrefab, machineDropPoint);
                item.Value.dropButtonRef = uiButtonObj.GetComponent<DropButtonController>();
                item.Value.dropButtonRef.Load(item.Value.mechPrefab, item.Value.count, NewSelection);
                item.Value.dropButtonRef.gameObject.SetActive(false);
            }
        }



        CheckForNextDictionary();

        resetMan.onPlay += onPlay;
        resetMan.onStop += onStop;

        stopButton.onClick.RemoveAllListeners();
        stopButton.onClick.AddListener(resetMan.StopAll);
    }

    void onPlay()
    {
        if (!resetMan.isDropComplete)
        {
            instance.stopButton.gameObject.SetActive(true);
        }
        instance.machineDropPoint.gameObject.SetActive(false);
        onNewMachineBeingPicked?.Invoke(MechType.NULL_MECH);
    }
    void onStop()
    {
        instance.stopButton.gameObject.SetActive(false);
        if (!resetMan.isDropComplete)
        {
            instance.machineDropPoint.gameObject.SetActive(true);
        }
    }

    Dictionary<MechType, MechDropData> dropDataDictionary;

    Dictionary<MechType, MechDropData> GetNextDictionary()
    {
        foreach (Dictionary<MechType, MechDropData> dic in dropDataDictionaries)
        {
            if (dic.Count <= 0) continue;
            else
            {
                foreach (var dropDataPair in dic)
                {
                    if (dropDataPair.Value.count > 0) return dic;
                    else continue;
                }
            }
        }
        return null;
    }

    void CheckForNextDictionary()
    {
        dropDataDictionary = GetNextDictionary();
        if (dropDataDictionary != null)
        {
            foreach (var item in dropDataDictionary)
            {
                item.Value.dropButtonRef.gameObject.SetActive(true);
            }
        }
        else
        {
            instance.machineDropPoint.gameObject.SetActive(false);
            resetMan.isDropComplete = true;
           // MachineResetManager.instance.PlayAll();
            PlayStopButtonController.FinalPlay(1.5f);
        }
    }

    public bool isDroppingShit = false;
    void Update()
    {


        if (Input.GetMouseButtonDown(0) || Input.GetMouseButton(0))
        {
            if (resetMan.isPlaying)
            {
                return;
            }
            else
            {
                OnDrop();
            }
        }
        else
        {
            if (isDroppingShit == true)
            {
                isDroppingShit = false;
                //DoVibrate((long)(1000 * 0.1f), -1);
            }
        }

    }

    public void OnDrop()
    {

        Ray ray = cam.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
        RaycastHit[] rcha = Physics.SphereCastAll(
            ray:ray,
            radius: sphereDiameter/2,
            maxDistance: 100,
            layerMask: mask,
            queryTriggerInteraction: QueryTriggerInteraction.Collide);

        foreach (var rch in rcha)
        {
            BaseMachineBehaviour bmb = rch.collider.GetComponentInParent<BaseMachineBehaviour>();

            onNewMachineChosen?.Invoke(bmb);
            if (bmb != null)
            {
                DoVibrate((long)(1000*0.35f),5);
                isDroppingShit = true;
            }
        }


    }

    void DoVibrate(long timeInMilliseconds = 1000, int intensity = 255)
    {
        if (Application.platform != RuntimePlatform.Android) return;
        AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject ca = unity.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaClass vibratorClass = new AndroidJavaClass("android.os.Vibrator");
        //AndroidJavaObject vibratorService = ca.GetStatic<AndroidJavaObject>("VIBRATOR_SERVICE");
        // AndroidJavaObject  vibratorService = ca.Call<AndroidJavaObject>("getSystemService", "VIBRATOR_SERVICE");
         AndroidJavaObject vibratorService = ca.Call<AndroidJavaObject>("getSystemService", ca.GetStatic<AndroidJavaObject>("VIBRATOR_SERVICE"));

        AndroidJavaClass vibrationEffect = new AndroidJavaClass("android.os.VibrationEffect");
        AndroidJavaObject vibrationDetails = vibrationEffect.CallStatic<AndroidJavaObject>("createOneShot", timeInMilliseconds, intensity);




        vibratorService.Call("vibrate", vibrationDetails);
        unity.Dispose();
        ca.Dispose();
        vibratorClass.Dispose();
        vibratorService.Dispose();
        vibrationEffect.Dispose();
        vibrationDetails.Dispose();
    }

    public static void DropSuccess(MechType type)
    {
        MechDropData mdd = instance.dropDataDictionary[type];

        mdd.count--;
        if (mdd.count > 0)
        {
            mdd.dropButtonRef.ChangeCount(mdd.count);
        }
        else
        {
            mdd.dropButtonRef.gameObject.SetActive(false);
            bool allDone = true;
            foreach (var item in instance.dropDataDictionary)
            {
                if (item.Value.count > 0) allDone = false;
            }
            if (allDone)
            {

                instance.CheckForNextDictionary();
            }
        }
    }

    public void NewSelection(MechType type)
    {
        onNewMachineBeingPicked?.Invoke(type);
    }

    public static int TypeToGroup(MechType type)
    {
        return 0;
        if ((int)type <= 100)
        {
            return 0;
        }
        else if ((int)type <= 200)
        {
            return 1;
        }
        else
        {
            return 2;
        }
    }
}
public class MechDropData
{
    public int count;

    public BaseMachineBehaviour mechPrefab;
    public DropButtonController dropButtonRef;
    public MechDropData(int count, BaseMachineBehaviour mechPrefab)
    {
        this.count = count;
        this.mechPrefab = mechPrefab;
    }
}
public enum MechType
{
    NULL_MECH = -1,


    FLAT_FORM = 51,
    TURN_FORM = 52,
    RAMP_FORM_1 = 61,
    RAMP_FORM_2 = 62,

    TRAMPOLINE = 101,
    SPRING = 102,

    LEVER_TILT = 111,
    HAMMERED_PUNCHUP = 121,


    BALL_A = 501,

    DOMINO_STRAIGHT_4 = 601,
    DOMINO_TURN_4 = 602,

}