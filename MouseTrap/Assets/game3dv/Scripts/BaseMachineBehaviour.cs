﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseMachineBehaviour : MonoBehaviour
{
    public bool log = false;
    public MechType type;
    public GameObject machineRoot;
    public GameObject placementCopy;
    public Transform uiPlacementTransform;


    MachineTransformState initialTrans;
    MachineTransformState savedTrans;

    MachineRGBDState initialRGBD;

    //public OverlapDetection overlapDetection;

    public bool isDisplay = false;
    public bool isPlacable = true;
    public bool isMachineElementsEnabled
    {
        get
        {
            return machineRoot.activeSelf;
        }
        set
        {
            machineRoot.SetActive(value);
            if(value)placementCopy.SetActive(false);
        }
    }

    public bool isHighlighted
    {
        get
        {
            return placementCopy.activeSelf;
        }
        set
        {
            placementCopy.SetActive(value);
        }
    }

    Outline outline;
    
    public Rigidbody rgbd;
    public Animator anim_goreset;

    public Transform targetTrans
    {
        get
        {
            return machineRoot.transform;
        }
    }


    void Start()
    {
        if (isDisplay)
        {
            OnReset();
            isHighlighted = false;
            
            return;
        }
        //outline = this.machineRoot.AddComponent<Outline>();
        //outline.OutlineMode = Outline.Mode.OutlineAll;
        //outline.OutlineColor = ColorRefManager.instance.unlockedMachine;
        //outline.OutlineWidth = 4.2f;


        if(isPlacable)MachineDropManager.instance.onNewMachineBeingPicked += OnNewMachineBeingPicked;
        if (isPlacable) MachineDropManager.instance.onNewMachineChosen += OnNewMachineChosen;

        if (isPlacable)
        {
            isMachineElementsEnabled = false;
            isHighlighted = false;
        }
        else
        {

            isMachineElementsEnabled = true;
            Init();
        }

    }
    public void OnNewMachineBeingPicked(MechType mtype)
    {
        if (isMachineElementsEnabled) return;
        if (isPlacable)
        {
            if (mtype == type)
            {

                isHighlighted = true;
            }
            else if (isHighlighted)
            {
                isHighlighted = false;
            }
        }
    }
    public void OnNewMachineChosen(BaseMachineBehaviour bmb)
    {
        if (isMachineElementsEnabled) return;
        if (isHighlighted && bmb == this)
        {
            isMachineElementsEnabled = true;
            Init();
            OnReset();

            StartCoroutine(OnChosen());
        }
    }
    IEnumerator OnChosen()
    {
        Vector3 startPos;

        Vector3 targetPos;
        Vector3 initialScale ;
        Vector3 maxScale ;
        float startTime;
        float allotedTime;

        startTime = Time.time;
        allotedTime = 0.0f;
        startPos = this.transform.position + new Vector3(0, 0.5f, 0);
        targetPos = this.transform.position + new Vector3(0,1.0f,0);
        initialScale = this.transform.localScale;
        maxScale = this.transform.localScale * 1.15f;
        this.transform.position = startPos;
        while (Time.time<startTime+allotedTime)
        {
            float lerpVal = Mathf.Clamp01((Time.time - startTime) / allotedTime);
            this.transform.position = Vector3.Lerp(startPos,targetPos,lerpVal);
            this.transform.localScale = Vector3.Lerp(initialScale,maxScale,lerpVal);
            yield return null;
        }
        this.transform.position = targetPos;
        this.transform.localScale = maxScale;


        startTime = Time.time;
        allotedTime = 0.3f;
        startPos = this.transform.position;
        targetPos = this.transform.position + new Vector3(0, -1.0f, 0);
        while (Time.time < startTime + allotedTime)
        {
            float lerpVal = Mathf.Clamp01((Time.time - startTime) / allotedTime);
            this.transform.position = Vector3.Lerp(startPos, targetPos, lerpVal);
            this.transform.localScale = Vector3.Lerp(maxScale, initialScale, lerpVal);
            yield return null;
        }
        this.transform.position = targetPos;
        this.transform.localScale = initialScale;

        MachineDropManager.DropSuccess(type);
    }


    bool initialized = false;
    void Init()
    {
        if (initialized) return;
        initialized = true;
        //if (log) Debug.Log("init");
        OnInit();
        MachineResetManager.instance.onReset += OnReset;
        MachineResetManager.instance.onPlay += OnPlay;
        MachineResetManager.instance.onStop += OnStop;

    }
    private void OnDestroy()
    {
        if (isPlacable) MachineDropManager.instance.onNewMachineBeingPicked -= OnNewMachineBeingPicked;
        if (isPlacable) MachineDropManager.instance.onNewMachineChosen -= OnNewMachineChosen;

        if (!initialized) return;
        MachineResetManager.instance.onReset -= OnReset;
        MachineResetManager.instance.onPlay -= OnPlay;
        MachineResetManager.instance.onStop -= OnStop;
    }
    //void ScheduleStop()
    //{
    //    stopScheduled = true;
    //}
    //void SchedulePlay()
    //{
    //    playScheduled = true;
    //}
    //void ScheduleReset()
    //{
    //    resetScheduled = true;
    //}

    public virtual void OnInit()
    {
        //if (log) Debug.LogFormat("On init has initial trans : {0}", initialTrans!=null);
        if (initialTrans!=null) return;
        initialTrans = new MachineTransformState(machineRoot.transform,log, "on_init");

        if (rgbd)
        {
            initialRGBD = new MachineRGBDState(rgbd);
            rgbd.isKinematic = true;
        }
    }
    public virtual void OnReset()
    {
        //resetScheduled = false;
        if (initialTrans == null) OnInit();
        initialTrans.ApplyState(machineRoot.transform);

        if (rgbd) rgbd.isKinematic = true;
        if (anim_goreset)
        {
            anim_goreset.ResetTrigger("go");
            anim_goreset.SetTrigger("reset");
        }
    }
    
    public virtual void OnPlay()
    {
        //playScheduled = false;

        savedTrans = new MachineTransformState(machineRoot.transform,log, "on_play");
        if (rgbd) initialRGBD.ApplyState(rgbd);
    }
    public virtual void OnStop()
    {
        //stopScheduled = false;

        if (savedTrans == null)
        {
            OnReset();
            return;
        }

        if (rgbd) { rgbd.isKinematic = true;
            rgbd.velocity = Vector3.zero;
        }
        if (anim_goreset)
        {
            anim_goreset.ResetTrigger("go");
            anim_goreset.SetTrigger("reset");
        }
        savedTrans.ApplyState(machineRoot.transform);



    }

    //public float colorLerpRate = 1;
    private void Update()
    {
        //if (outline && outline.enabled != isPlayerPlacable)
        //{
        //    outline.enabled = isPlayerPlacable;
        //}

        OnUpdate();
    }


    //bool playScheduled;
    //bool stopScheduled;
    //bool resetScheduled;
    //private void FixedUpdate()
    //{

    //    if (playScheduled) OnPlay();
    //    if (stopScheduled) OnStop();
    //    if (resetScheduled) OnReset();

    //    OnFixedUpdate();
    //}
    //public virtual void OnFixedUpdate()
    //{

    //}
    public virtual void OnUpdate()
    {

    }

}
[System.Serializable]
public class MachineTransformState
{
    public Vector3 position;
    public Quaternion rotation;
    public Vector3 scale;

    bool log=false;
    string tag="";
    public MachineTransformState() { }
    public MachineTransformState(Transform tr, bool log, string tag)
    {
        position = tr.localPosition;
        rotation = tr.localRotation;
        scale = tr.localScale;
        this.log = log;
        this.tag = tag;
        //if (log) Debug.LogFormat("created {0}: {1}",tag,position);
    }

    public void ApplyState(Transform tr)
    {
        tr.localPosition = position;
        tr.localRotation = rotation;
        tr.localScale = scale;
        if (log)
        {
            //Debug.LogFormat("loaded {0}: {1}", tag, position);
           // Debug.Break();
        }
    }
}

[System.Serializable]
public class MachineRGBDState
{
    public bool isKinamatic;
    public MachineRGBDState() { }
    public MachineRGBDState(Rigidbody rgbd)
    {
        isKinamatic = rgbd.isKinematic;
    }

    public void ApplyState(Rigidbody rgbd)
    {
        rgbd.isKinematic = isKinamatic;
    }
}


