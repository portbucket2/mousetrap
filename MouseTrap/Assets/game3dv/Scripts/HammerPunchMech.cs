﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HammerPunchMech : BaseMachineBehaviour
{
    public AutoDamper damper;
    public override void OnStop()
    {
        base.OnStop();
        anim.SetTrigger("reset");
    }

    public Animator anim;
    private void OnTriggerEnter(Collider other)
    {
        Rigidbody rgbd = other.GetComponentInParent<Rigidbody>();
        damper.shouldDamp = false;

        FishSpace.Centralizer.Add_DelayedMonoAct(damper, () => damper.shouldDamp = true, 1, false);

        if (rgbd && !rgbd.isKinematic)
        {
            anim.ResetTrigger("reset");
            anim.SetTrigger("go");
        }
    }
}
