﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Basic2StageTutorial : MonoBehaviour
{
    public GameObject stage1;
    public GameObject stage2;
    public Text tutText;
    public string firstInstruction = "Tap to select";
    public string secondInstruction = "tap to place";


    bool unsub2;
    private void Start()
    {
        FindObjectOfType<RotationManager>().enabled = false;
        MachineDropManager.instance.onNewMachineBeingPicked += MachineTapped;
        PlayStopButtonController.instance.onFinalPlayCountDownStart += MachineDropped;
        stage1.SetActive(true);
        stage2.SetActive(false);
        
    }


    private void OnDestroy()
    {
        if (!finalPlayUnsubbed)
        {

            PlayStopButtonController.instance.onFinalPlayCountDownStart -= MachineDropped;
            //ebug.Log("unsub on destroy");
        }
    }
    private void MachineTapped(MechType type)
    {
        stage1.SetActive(false);
        stage2.SetActive(true);
        tutText.text = secondInstruction;
        MachineDropManager.instance.onNewMachineBeingPicked -= MachineTapped;
    }

    bool finalPlayUnsubbed = false;
    private void MachineDropped()
    {
        PlayStopButtonController.instance.onFinalPlayCountDownStart -= MachineDropped;
        finalPlayUnsubbed = true;
        stage2.SetActive(false);
        tutText.gameObject.SetActive(false);
        this.gameObject.SetActive(true);
    }
}
