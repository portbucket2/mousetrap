﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverArmController : MonoBehaviour
{
    public Animator anim;
    public OverlapDetection overlapD;
    public Vector3 endVelocity;
    public AutoDamper damper;

    private void OnCollisionEnter(Collision collision)
    {
        Rigidbody rgbd = collision.collider.GetComponentInParent<Rigidbody>();

        if (rgbd && !rgbd.isKinematic)
        {
            anim.SetTrigger("go");
            damper.shouldDamp = false;

            FishSpace.Centralizer.Add_DelayedMonoAct(damper,()=> damper.shouldDamp =true, 1,false);
            foreach (var item in overlapD.items)
            {
                Rigidbody rigid = item.attachedRigidbody;

                if (rigid&&!rigid.isKinematic)
                {
                    rigid.velocity = this.GetComponentInParent<BaseMachineBehaviour>().transform.rotation*endVelocity;
                    //Debug.Log("fire!");
                }
            }
        }
    }

    
}
