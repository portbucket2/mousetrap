﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpringMech : BaseMachineBehaviour
{
    //public bool log;
    public Vector3 velChange = new Vector3(0,5,0);
    //public bool resetOldVel = false;

    public GameObject triggerObject;

    public override void OnReset()
    {
        base.OnReset();
        triggerObject.SetActive(true);
    }

    public override void OnStop()
    {
        base.OnStop();
        triggerObject.SetActive(true);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (isDisplay) return;
        EnforcableMachine em = other.gameObject.GetComponentInParent<EnforcableMachine>();
        if (em)
        {
            Vector3 v = em.rgbd.velocity;
            float vx = Mathf.Abs(v.x);
            float vz = Mathf.Abs(v.z);


            if (log) Debug.LogFormat("entryPos {0}, vel: {1}", other.transform.position,v );

            if (vx > vz && vz!=0)
            {
                //if (log) Debug.LogFormat("z nutralized {0}", v.z);
                v.z = 0;
                em.rgbd.velocity = v;

                Vector3 p = em.transform.position;
                p.z = this.transform.position.z;
                em.transform.position = p;
            }
            else if (vx < vz && vx != 0)
            {
                //if (log) Debug.LogFormat("x nutralized {0}", v.x);
                v.x = 0;
                em.rgbd.velocity = v;

                Vector3 p = em.transform.position;
                p.x = this.transform.position.x;
                em.transform.position = p;
            }


            em.rgbd.SetHorizontalSpinByVelocity(2);
            //if (Mathf.Abs(v.x) > 0)
            //{
            //    em.rgbd.angularVelocity = new Vector3(0, 0, v.x);
            //    //Debug.Log("spring A");
            //}
            //else if (Mathf.Abs(v.z) > 0)
            //{
            //    em.rgbd.angularVelocity = new Vector3(v.z, 0,0);
            //    //Debug.Log("spring B");
            //}

            //if (resetOldVel) em.rgbd.velocity = Vector3.zero;



            em.rgbd.AddForce(velChange, ForceMode.VelocityChange);

            anim_goreset.SetTrigger("go");
            triggerObject.SetActive(false);
        }
    }
}
