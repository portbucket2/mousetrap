﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelPrefabManager : MonoBehaviour
{

    public static LevelPrefabManager currentLevel;
    public int areaI { get; private set; }
    public int levelI { get; private set; }

    int stars = 0;

    System.Action onEnd;

    public InLevelCanvasManager canvMan
    {
        get
        {
            return MainGameManager.instance.levelCanvas;
        }
    }

    public void Load(int areaIndex, int levelIndex, System.Action onEnd)
    {
        currentLevel = this;
        areaI = areaIndex;
        levelI = levelIndex;
        this.onEnd = onEnd;
        
        canvMan.LevelStart(this);
    }

    public void UnLoad()
    {
        currentLevel = null;
        Destroy(gameObject);
        onEnd?.Invoke();
    }

    public void ReportEarlyCompletion (int reportedStars)
    {
        int starC = LevelLoader.instance.GetStarFor(areaI, levelI);
        if (starC < reportedStars)
        {
            LevelLoader.instance.SetStarFor(areaI, levelI, reportedStars);
        }
        if (LevelLoader.instance.levelAreas[areaI].lastUnlockedLevelIndex.value == levelI)
        {
            LevelLoader.instance.levelAreas[areaI].lastUnlockedLevelIndex.value++;
        }
    }

    public void OnComplete(bool success)
    {
        if (success)
        {
            Handheld.Vibrate();
            int starC = LevelLoader.instance.GetStarFor(areaI,levelI);
            if (starC >= stars)
            {
                stars = starC;
            }
            else
            {
                LevelLoader.instance.SetStarFor(areaI,levelI,stars);
            }
            if (LevelLoader.instance.levelAreas[areaI].lastUnlockedLevelIndex.value == levelI)
            {
                LevelLoader.instance.levelAreas[areaI].lastUnlockedLevelIndex.value++;
            }
            canvMan.LoadSucces();
        }
        else
        {
            canvMan.LoadFail();
        }
        canvMan.SetStars(stars);
    }

    public void AddStar()
    {
        stars++;
        canvMan.SetStars(stars);
    }

    public void OnNext()
    {
        UnLoad();
        if (levelI < LevelLoader.instance.levelAreas[areaI].levelPrefs.Count - 1)
        {
            MainGameManager.instance.StartLevel(areaI, levelI+1);
        }
        
    }
    public void OnReset()
    {
        UnLoad();
        MainGameManager.instance.StartLevel(areaI,levelI);
    }



    public void OnLevels()
    {
        UnLoad();
    }
}
