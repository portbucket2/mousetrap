﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InLevelCanvasManager : MonoBehaviour
{
    public GameObject successObj;
    public GameObject failureObj;

    public List<GameObject> stars;



    public Button restartButton;
    public Button levelsButton;

    public Button levels2Button;
    public Button replayButton;
    public Button nextButton;

    public Button restartButtonExt;
    public Button levelsButtonExt;

    private LevelPrefabManager levelObject;
    public Text levelNameText;
    public  void LevelStart(LevelPrefabManager levelObj)
    {
        levelObject = levelObj;
        failureObj.SetActive(false);
        successObj.SetActive(false);
        //restartButtonExt.gameObject.SetActive(true);
        //levelsButtonExt.gameObject.SetActive(true);

        levelNameText.text = LevelLoader.GetLevelName(levelObj.areaI,levelObj.levelI);

        nextButton.onClick.RemoveAllListeners();
        nextButton.onClick.AddListener(levelObject.OnNext);

        //if (levelObject.levelI < LevelLoader.instance.levelAreas[levelObject.areaI].levelPrefs.Count - 1)
        //{
        //    nextButton.gameObject.SetActive(true);

        //}
        //else
        //{
        //    nextButton.gameObject.SetActive(false);
        //}


        restartButton.onClick.RemoveAllListeners();
        restartButton.onClick.AddListener(levelObject.OnReset);

        replayButton.onClick.RemoveAllListeners();
        replayButton.onClick.AddListener(levelObject.OnReset);

        levelsButton.onClick.RemoveAllListeners();
        levelsButton.onClick.AddListener(levelObject.OnLevels);

        levels2Button.onClick.RemoveAllListeners();
        levels2Button.onClick.AddListener(levelObject.OnLevels);


        restartButtonExt.onClick.RemoveAllListeners();
        restartButtonExt.onClick.AddListener(levelObject.OnReset);

        levelsButtonExt.onClick.RemoveAllListeners();
        levelsButtonExt.onClick.AddListener(levelObject.OnLevels);
        SetStars(0);
    }





    public void SetStars(int starCount)
    {
        for (int i = 0; i < stars.Count; i++)
        {
            stars[i].SetActive(starCount > i);
        }
    }


    public void LoadFail()
    {
        failureObj.SetActive(true);
        //restartButtonExt.gameObject.SetActive(false);
    }
    public void LoadSucces()
    {
        successObj.SetActive(true);
        //restartButtonExt.gameObject.SetActive(false);
    }
}
