﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorRefManager : MonoBehaviour
{
    public Color unlockedMachine;
    public Color draggingNormalMachine;
    public Color draggingOverlapMachine;

    public static ColorRefManager instance;

    private void Awake()
    {
        instance = this;
    }
}
