﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PointerPressedDetect : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, ISelectHandler, IDeselectHandler
{
    public float fristChangeTarget = 1.15f;
    public float secondChangeTarget = 0.8f;
    [Space]
    public float firstChangeSpeed = 25;
    public float secondChangeSpeed = 25;
    public float restoreSpeed = 65;
    [Space]
    public float firstToSecondTime = 0.1f;
    public float restoreMaxTime = 0.1f;

    private void Awake()
    {
        targetScale = Vector3.one;
        lerpRate = firstChangeSpeed * Time.deltaTime;
        if (outObject) outObject.SetActive(false);
    }
    Vector3 targetScale;
    Vector3 targetLocalPos;
    float lerpRate;

    public void OnPointerUp(PointerEventData eventData)
    {
        targetScale = Vector3.one * secondChangeTarget;

        lerpRate = secondChangeSpeed * Time.deltaTime;
        FishSpace.Centralizer.Add_DelayedMonoAct(this, () => {
            targetScale = Vector3.one;
            lerpRate = restoreSpeed * Time.deltaTime;
        }, firstToSecondTime, true);

        FishSpace.Centralizer.Add_DelayedMonoAct(this, () => {
            lerpRate = firstChangeSpeed * Time.deltaTime;
        }, firstToSecondTime+restoreMaxTime, true);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        targetScale = Vector3.one * fristChangeTarget;
    }
    void Update()
    { 
        transform.localScale = Vector3.Lerp(transform.localScale, targetScale, lerpRate);
    }


    public GameObject outObject;
    public void OnDeselect(BaseEventData eventData)
    {
        if (outObject)
            outObject.SetActive(false);
    }

    public void OnSelect(BaseEventData eventData)
    {
        if (outObject)
            outObject.SetActive(true);
    }
}
