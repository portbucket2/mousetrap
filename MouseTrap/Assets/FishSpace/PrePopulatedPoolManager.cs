﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishSpace;

public class PrePopulatedPoolManager : MonoBehaviour
{
    static List<GameObject> prepopulatedOrignals = new List<GameObject>();

    public List<PrePopulationPoolSets> sets;
    

    private void Awake()
    {
        //if(prepopulatedOrignals.Count>0)
        //{
        //    Destroy(this.gameObject);
        //    return;
        //}

        //DontDestroyOnLoad(this.gameObject);

        foreach (PrePopulationPoolSets prePopSet in sets)
        {
            //if (!prepopulatedOrignals.Contains(prePopSet.original))
            //{
            //prepopulatedOrignals.Add(prePopSet.original);
            FishSpace.Pool.PrePopulatePool(prePopSet.original, prePopSet.prePopulation);
            //}
        }
    }

}
[System.Serializable]
public class PrePopulationPoolSets
{
    public GameObject original;
    public PooledItem[] prePopulation;
}
