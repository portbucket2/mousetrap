﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoLeverController : MonoBehaviour 
{
    public AutoLeverTriggerController topTriggerCon;

    public float verticalForce;
    public float horizontalForce;

    public bool initialTiltIsRight;

    void Awake()
    {
        Reset();
    }

    internal void Reset()
    {
        anim.SetBool("right", initialTiltIsRight);
        _isTiltedRight = initialTiltIsRight;
    }

    public Animator anim;
     bool _isTiltedRight;
    public void SetTiltedRight(bool isRightNewValue, float mass)
    {
        if (isRightNewValue == _isTiltedRight) return;
        _isTiltedRight = isRightNewValue;
        anim.SetBool("right", isRightNewValue);

        float massMult = mass / 0.1f;

        foreach (var item in topTriggerCon.colliderList)
        {
            if (item.transform.position.x < this.transform.position.x && isRightNewValue)
            {
                item.GetComponentInParent<Rigidbody>().AddForce(new Vector3(-horizontalForce, verticalForce, 0)*massMult, ForceMode.Force);
            }
            if (item.transform.position.x > this.transform.position.x && !isRightNewValue)
            {
                item.GetComponentInParent<Rigidbody>().AddForce(new Vector3(horizontalForce, verticalForce, 0)*massMult, ForceMode.Force);
            }

        }
    }
    public bool GetTiltedRigth()
    {
        return _isTiltedRight;
    }



    private void OnCollisionEnter(Collision collision)
    {
        Vector3 hitPoint = collision.contacts[0].point;

        float xDiff = hitPoint.x - this.transform.position.x;



        //Debug.Log("xDiff");
        bool tiltShouldBeRight = xDiff > 0;

        float mass = collision.collider.GetComponentInParent<Rigidbody>().mass;

        SetTiltedRight(tiltShouldBeRight, mass);

    }


}
